{- Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
- Martínez Gutiérrez Antonio Daniel -}

uno = [1]
dos = [1,2]
tres = [1,2,3]
cuatro = [1,2,3,4]
list = [uno, uno, tres, dos, tres, cuatro, dos, uno]

{- Crea una lista por comprensión que dada una lista de listas, 
regresa las listas con longitud n. -}
listasDeN :: [[a]] -> Int -> [[a]]
listasDeN l n = [y | y <- l, length y == n]

{- Crea una lista por comprensión que dada una lista de listas, 
regresa las listas con longitud par concatenadas consigo mismas. -}
dobleListaComp :: [[a]] -> [[a]]
dobleListaComp l = [y ++ y | y <- l, mod (length y) 2 == 0]