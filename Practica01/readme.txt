Lógica computacional
Practica 01: Recordando Haskell

Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
- Martínez Gutiérrez Antonio Daniel

Desarrollo de la práctica:
- Nos organizamos por medio de un grupo, nos pusimos de acuerdo en quién sería el encargado de hacer el repositorio, nos repartimos las funciones para ir viendo el flujo de trabajo de varias personas en un mismo repositorio.

Dificultades:
- El manejo de gitlab y el flujo de trabajo para hacer cambios en el repositorio.
- Algunos conceptos de Haskell como instance y deriving en un principio fueron más complicados de entender e implementar.
- Aprender o recordar como programar en Haskell.
- Hallar la relación que mejor nos funcionara para hacer operaciones y conversiones de números binarios (especialmente resta).
