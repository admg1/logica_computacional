{- Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
- Martínez Gutiérrez Antonio Daniel -}

---Coloraciones
import Data.List 
data Color = Rojo | Amarillo | Verde | Azul deriving (Show, Eq)

data Balcanes = Albania
              | Bulgaria
              | BosniaYHerzegovina
              | Kosovo
              | Macedonia
              | Montenegro deriving (Show, Eq, Ord)

type Ady = [(Balcanes, Balcanes)]

adyacencias :: Ady
adyacencias = 
  [ (Albania, Montenegro), (Albania, Kosovo), (Albania, Macedonia),
    (Bulgaria, Macedonia), (BosniaYHerzegovina, Montenegro),
    (Kosovo, Macedonia), (Kosovo, Montenegro)
  ]

type Coloracion = [(Color, Balcanes)]

paisesColores :: Coloracion
paisesColores = [(Azul,Albania), (Verde, Kosovo), (Amarillo, Bulgaria), (Rojo, Macedonia), (Azul, Montenegro)]

-- Funcion que agrupa por colores
agrupaColor :: Ady -> Coloracion -> [[Balcanes]]
agrupaColor ady col = [[ snd x | x <- col, fst x == Rojo], -- rojo
               [ snd x | x <- col, fst x == Verde], -- verde
               [ snd x | x <- col, fst x == Amarillo], -- etc
               [ snd x | x <- col, fst x == Azul]]

-- Funcion para verificar si una adyacencia dada es Buena

esBuena :: Ady -> Coloracion -> Bool
esBuena ady col =
  let
    colores = [[ snd x | x <- col, fst x == Rojo],
               [ snd x | x <- col, fst x == Verde], 
               [ snd x | x <- col, fst x == Amarillo], 
               [ snd x | x <- col, fst x == Azul]]
  in
    verifica ady (creaTodasLasTuplasCOOL $ filter ((> 1) . length) colores)

-- Funcion que verifica si las tuplas recibidas se encuentran en las adyacencias,
-- recibe las tuplas de creaTodasTuplas
verifica :: Ady -> [(Balcanes, Balcanes)] -> Bool
verifica ady [] = True
verifica ady (x:xs)
  | elem x ady = False
  | otherwise  = verifica ady xs

-- Funcion que genera todas las posibles combinaciones de tuplas entre balcanes y colores
creaTodasLasTuplasCOOL :: [[b]] -> [(b, b)]
creaTodasLasTuplasCOOL [] = []
creaTodasLasTuplasCOOL (z:zs) = [(x,y) | x <- z, y <- z] ++ creaTodasLasTuplasCOOL zs

colores = [Rojo, Amarillo, Verde, Azul]
balcanes = [Albania, Bulgaria, BosniaYHerzegovina, Kosovo, Macedonia, Montenegro]

-- Funcion que obtiene las coloraciones buenas y completas en una adyacencia
coloraciones :: Ady -> [Coloracion]
coloraciones ad = filter (esBuena ad) coloresOne
    where  
        coloresOne = [[(c1,b1)]++[(c2,b2)]++ [(c3,b3)]++[(c4,b4)]++[(c5,b5)]++[(c6,b6)]  
                  | c1 <- colores,
                    c2 <- colores,
                    c3 <- colores,
                    c4 <- colores,
                    c5 <- colores,
                    c6 <- colores,
                    b1 <- balcanes,
                    b2 <- balcanes,
                    b3 <- balcanes,
                    b4 <- balcanes,
                    b5 <- balcanes,
                    b6 <- balcanes,
                    b1 /= b2, b1/=b3, b1/=b4,b1/=b5, b1/=b6,
                    b2 /= b3, b2/=b4, b2/=b5,b2/=b6,
                    b3 /= b4, b3/=b5, b3/=b6,
                    b4 /= b5, b4/=b6,
                    b5 /= b6,
                    b1 < b2, b2 < b3, b3 < b4, b4 < b5, b5 < b6,
                    b2 < b3, b2 < b4, b2 < b5, b2 < b6,
                    b3 < b4, b3 < b5, b4 < b6,
                    b4 < b5, b4 < b6,
                    b5 < b6]

--Se llama asi: coloraciones adyacencias

--Ejemplo de mala coloracion:
adyacenciasMalColor :: Ady
adyacenciasMalColor = 
  [ (Albania, Montenegro), (Albania, Kosovo), (Albania, Macedonia),
    (Bulgaria, Macedonia), (BosniaYHerzegovina, Montenegro),
    (Kosovo, Macedonia), (Kosovo, Montenegro)
  ]
coloresMalos :: Coloracion
coloresMalos = [(Azul,Albania), (Verde, Kosovo), (Amarillo, Bulgaria), (Rojo, Macedonia), (Azul, Montenegro)]


--Asi la verificamos
--esBuena adyacenciasMalColor coloresMalos


--Ejemplo de buena coloracion:
adyacenciasBuenColor :: Ady
adyacenciasBuenColor = 
  [ (Albania, Montenegro), (Albania, Kosovo), (Albania, Macedonia),
    (Bulgaria, Macedonia), (BosniaYHerzegovina, Montenegro),
    (Kosovo, Macedonia), (Kosovo, Montenegro)
  ]
coloresBien :: Coloracion
coloresBien = [(Azul,Albania), (Verde, Kosovo), (Amarillo, Bulgaria), (Rojo, Macedonia), (Amarillo, Montenegro)]


--Asi la verificamos
--esBuena adyacenciasBuenColor coloresBien



        


