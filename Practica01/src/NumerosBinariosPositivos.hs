{- Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
- Martínez Gutiérrez Antonio Daniel -}

-- Declaración del tipo de dato BinPos
data BinPos = U | Cero (BinPos) | Uno (BinPos)

-- Instacia de clasa Show para representar números binarios positivos
instance Show BinPos where
  show U = "1"
  show (Cero bp) = show bp ++ "0"
  show (Uno bp) = show bp ++ "1"

-- Tests de instance Show.
instance1 = show (Uno (Cero (Uno (Cero (Cero (U))))))
--Regresa: "100101"
instance2 = show (Cero (Uno (Cero (Uno (U)))))
--Regresa: "11010"

-- Función que dado un BinPos devuelve a su sucesor
sucesor :: BinPos -> BinPos
sucesor U = Cero U
sucesor (Cero x) = (Uno x)
sucesor (Uno x) = Cero (sucesor x)

-- Tests de sucesor.
sucesor1 = sucesor (Cero (Cero (Uno (U))))
-- Regresa: 1101
sucesor2 = sucesor (Cero (Uno( Uno (Cero (Uno (U))))))
-- Regresa: 110111

-- Función que dados dos BinPos devuelve su suma
suma :: BinPos -> BinPos -> BinPos
suma U a = sucesor a
suma a U = sucesor a
suma (Cero bp1) (Cero bp2) = Cero (suma bp1 bp2)
suma (Cero bp1) (Uno bp2) = Uno (suma bp1 bp2)
suma (Uno bp1) (Cero bp2) = Uno (suma bp1 bp2)
suma (Uno bp1) (Uno bp2) = Cero (suma U (suma bp1 bp2))

-- Tests de suma.
suma1 = suma (Cero (Cero (Uno (Uno (U))))) (Uno (Cero (Cero (Uno (Cero (U))))))
-- Regresa: 1000101
suma2 = suma (Cero (Uno (Cero (Uno (Uno (Uno (U))))))) (Cero (Uno (Cero (U))))
-- Regresa: 10000100

-- Función que dados dos BinPos devuelve su resta
resta :: BinPos -> BinPos -> BinPos
resta bp1 bp2 = if (digitosBinPos bp1 > digitosBinPos bp2)
  then restaAux bp1 bp2
  else U

-- Función auxiliar de resta (resta a un BinPos mayor un BinPos menor).
restaAux :: BinPos -> BinPos -> BinPos
restaAux (Cero U) U = U
restaAux (Cero bp1) U = Uno (resta bp1 U)
restaAux (Uno bp1) U = Cero bp1
restaAux (Cero bp1) (Cero bp2) = Cero (resta bp1 bp2)
restaAux (Cero bp1) (Uno bp2) = Uno (resta bp1 (sucesor bp2))
restaAux (Uno bp1) (Cero bp2) = Uno (resta bp1 bp2)
restaAux (Uno bp1) (Uno bp2) = Cero (resta bp1 bp2)

-- Función auxiliar de restaAux que devuelve el valor de un BinPos bajo el tipo Int.
digitosBinPos :: BinPos -> Int
digitosBinPos U = 1
digitosBinPos (Cero bp) = 1 + digitosBinPos bp
digitosBinPos (Uno bp) = 1 + digitosBinPos bp

-- Tests de resta.
resta1 = resta (Cero (Uno (Uno (U)))) (Uno (Cero (U)))
-- Regresa: 1001
resta2 = resta (Cero (Uno (Cero (Cero (U))))) (Uno (Uno (Uno (U))))
-- Regresa: 11

-- Función que dados dos BinPos devuelve su producto
producto :: BinPos -> BinPos -> BinPos
producto U x = x
producto x U = x
producto (Cero x) (Cero y)    = Cero (Cero (producto x y))
producto a@(Uno x)  b@(Uno y) = suma a (Cero (producto a y))
producto a@(Cero x) b@(Uno y) = suma a (Cero (producto a y))
producto a b = producto b a

-- Tests de producto.
producto1 = producto (Cero (Cero (U))) (Uno (Cero (Cero (Uno (U)))))
-- Regresa: 1100100
producto2 = producto (Cero (Uno (Uno (Uno (U))))) (Cero (Cero (Uno (Cero (U)))))
-- Regresa: 1001011000


-- Función que dado un BinPos devuelve el número natural representable bajo el tipo Int.
binPosToInt :: BinPos -> Int
binPosToInt U = 1
binPosToInt (Cero x) = binPosToInt(x) * 2
binPosToInt (Uno x) = 1 + binPosToInt(x) * 2

-- Tests de binPosToInt.
int1 = binPosToInt (Cero (Uno (Uno (Cero (Uno U)))))
-- Regresa: 54
int2 = binPosToInt (Uno (Uno (Uno (Cero U))))
-- Regresa: 23

-- Función que dado un número natural de tipo Int devuelve su representación bajo el tipo BinPos.
intToBinPos :: Int -> BinPos
intToBinPos 1 = U
intToBinPos n
 | mod n 2 == 0 = Cero (intToBinPos(div n 2))
 | otherwise = Uno (intToBinPos(div n 2))

-- Tests de intToBinPos.
bin1 = intToBinPos 16
-- Regresa: 10000
bin2 = intToBinPos 145
-- Regresa: 10010001
