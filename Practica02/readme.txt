Lógica computacional
Practica 02: Introducción a la lógica.

Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
- Martínez Gutiérrez Antonio Daniel

Desarrollo de la práctica y dificultades:

Para esta práctica, durante la primer semana se intentaron las primeras tres funciones
ya que el ayudante nos mencionó que serían las más sencillas, una vez que teníamos esas,
durante la segunda semana nos repartimos las funciones restantes.

Durante la segunda semana, la primera con la que tuvimos problemas fue con sustituye,
pues no teníamos claro si una vez que el nombre de una variable era sustituido se podía
volver a cambiar o se ignoraban las siguientes sustituciones donde se pretendía cambiar
la misma variable, el ayudante mencionó que se tomaba en cuenta únicamente la primer sustitución.

Luego otra función en la cual dudamos era en la de varList, pues al principio la función
regresaba todas las variables y si se repetían las regresaba también, sin embargo,
el ayudante mencionó que la lista de variables no tendría que tener repetidas, esto se
solucionó usando la función union que nos recomendó utilizar el ayudante.

Finalmente, con la función auxiliar estados que sirve para tablaDeVerdad, fue tal vez la más tardada
porque nunca se nos ocurrió anidar maps, pero nos ayudó el video que subió Emiliano, otra parte de
esta función que se nos dificultó fue concatenar los elementos una vez que teníamos la lista
con las variables con valor verdadero, y la otra que tenía las variables con valor falso, para esto
se creó la función auxiliar concatenaElementos, que resultó algo sencillo una vez que encontramos
la solución. Asimismo, después vimos que en el video vimos que se utilizó una función que desconociamos
la cual era zipWith, pero al realizar las comparaciones nuestra implementación también funcionó.

En general, esas fueron las dificultades que se nos presentaron al hacer las funciones, pero
las pudimos aclarar preguntando al ayudante o intentando otras implementaciones. Igualmente, fue más
sencillo el flujo de trabajo con git y su manejo. Por lo tanto, en términos generales la práctica nos
resultó más sencilla que la primera, pues ya estabamos más familiarizados con el lenguaje y el entorno
de trabajo.
