{-
- Lógica Computacional 2022-2
- Profesor: Francisco Hernández Quiroz
- Ayudante: José Ricardo Desales Santos
- Ayudante: Andrea Regina García Correa
- Laboratorio: Emiliano Galeana Araujo
- Laboratorio: César Eduardo Jardines Mendoza
- Practica 2: Lógica proposicional.
- Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
- Martínez Gutiérrez Antonio Daniel
-}

module LogPro where

import Data.List ()
import Data.List

data Prop =
  Var Name
  | Neg Prop
  | Conj Prop Prop
  | Disy Prop Prop
  | Impl Prop Prop
  | Syss Prop Prop

-- Variables.
type Name = String

-- Sustitución.
type Sust = [(Name, Name)]

-- Estado de una variable.
type Estado = (Name, Bool)

-- Solamente muchos estados.
type Estados = [Estado]

-- Un renglon es una lista de variables con su asignacion (True/False)
-- y un resultado.
type Renglon = (Estados, Bool)

-- Una tabla es solo una lista de renglones.
type Tabla = [Renglon]

-- Instancia para Show
instance Show Prop where
  show (Var x)    = show x
  show (Neg x)    = "¬"++ (show x)
  show (Conj x y) = "(" ++ show x ++ " Λ " ++ show y ++ ")"
  show (Disy x y) = "(" ++ show x ++ " ∨ " ++ show y ++ ")"
  show (Impl x y) = "(" ++ show x ++ " → " ++ show y ++ ")"
  show (Syss x y) = "(" ++ show x ++ " ↔ " ++ show y ++ ")"

-- Instancia para Eq
instance Eq Prop where
  (Var x)    == (Var y)      = x == y
  (Neg x)    == (Neg y)      = x == y
  (Conj x y) == (Conj x' y') = (x == x') && (y == y')
  (Disy x y) == (Disy x' y') = (x == x') && (y == y')
  (Impl x y) == (Impl x' y') = (x == x') && (y == y')
  (Syss x y) == (Syss x' y') = (x == x') && (y == y')
  _ == _ = False

--------------------------------------------------------------------------------
--------                            FUNCIONES                           --------
--------------------------------------------------------------------------------

-- | Funcion que saca todos los átomos de una proposicion.
varList :: Prop -> [Name]
varList (Var x) = [x]
varList (Neg x) = varList x
varList (Conj x y) = union (varList x) (varList y)
varList (Disy x y) = union (varList x) (varList y)
varList (Impl x y) = union (varList x) (varList y)
varList (Syss x y) = union (varList x) (varList y)


-- | Funcion que elimina las implicaciones y dobles implicaciones de
-- una proposicion.
equivalencia :: Prop -> Prop
equivalencia (Var x) = Var x
equivalencia (Neg x) = Neg (equivalencia x)
equivalencia (Conj x y) = Conj (equivalencia x) (equivalencia y)
equivalencia (Disy x y) = Disy (equivalencia x) (equivalencia y)
equivalencia (Impl x y) = Disy (equivalencia (Neg x)) (equivalencia y)
equivalencia (Syss x y) = equivalencia (Conj (Impl x y) (Impl y x))

-- | Funcion que niega una proposicion
negacion :: Prop -> Prop
negacion (Var x) = Neg (Var x)
negacion (Neg x) = x
negacion (Conj x y) = Disy (negacion x) (negacion y)
negacion (Disy x y) = Conj (negacion x) (negacion y)
negacion (Impl x y) = Conj x (negacion y)
negacion (Syss x y) = Disy (negacion (Impl x y)) (negacion (Impl y x))

-- | Funcion que dada una proposicion y una sustitucion, sustituye las
-- variables que correspondan.
sustituye :: Prop -> Sust -> Prop
sustituye (Var x) [] = Var x
sustituye (Var x) ((y, z):xs) =
  if y == nombre (Var x) then Var z
  else sustituye (Var x) xs
sustituye (Neg x) [] = Neg x
sustituye (Neg x) ((y, z):xs) = Neg (sustituye x ((y, z):xs))
sustituye (Conj x y) [] = Conj x y
sustituye (Conj x y) ((w, z):xs) = Conj (sustituye x ((w, z):xs)) (sustituye y ((w, z):xs))
sustituye (Disy x y) [] = Disy x y
sustituye (Disy x y) ((w, z):xs) = Disy (sustituye x ((w, z):xs)) (sustituye y ((w, z):xs))
sustituye (Impl x y) [] = Impl x y
sustituye (Impl x y) ((w, z):xs) = Impl (sustituye x ((w, z):xs)) (sustituye y ((w, z):xs))
sustituye (Syss x y) [] = Syss x y
sustituye (Syss x y) ((w, z):xs) = Syss (sustituye x ((w, z):xs)) (sustituye y ((w, z):xs))

-- | Funcion que dada una proposición y estados, evalua la proposicion
-- asignando el estado que corresponda. Si no existe una variable,
-- maneja el error.
interp :: Prop -> Estados -> Bool
interp (Var x) [] = error "No se encontró el estado para evaluar"
interp (Var x) ((w, z):xs) =
  if w == nombre (Var x) then z
  else interp (Var x) xs
interp (Neg x) [] = error "No se encontró el estado para evaluar"
interp (Neg x) ((w, z):xs) = not (interp x ((w, z):xs))
interp (Conj x y) [] = error "No se encontró el estado para evaluar"
interp (Conj x y) ((w, z):xs) = (interp x ((w, z):xs)) && (interp y ((w, z):xs))
interp (Disy x y) [] = error "No se encontró el estado para evaluar"
interp (Disy x y) ((w, z):xs) = (interp x ((w, z):xs)) || (interp y ((w, z):xs))
interp (Impl x y) [] = error "No se encontró el estado para evaluar"
interp (Impl x y) ((w, z):xs) = interp (equivalencia (Impl x y)) ((w, z):xs)
interp (Syss x y) [] = error "No se encontró el estado para evaluar"
interp (Syss x y) ((w, z):xs) = interp (equivalencia (Syss x y)) ((w, z):xs)

-- | Funcion que dada una proposicion, dice True si es tautologia,
-- False en otro caso.
esTautologia :: Prop -> Bool
esTautologia p = (2 ^ (length (varList p))) == length (modelos p)

-- | Funcion que dada una proposicion, dice True si es una
-- contradiccion, False en otro caso.
esContradiccion :: Prop -> Bool
esContradiccion p = modelos p == []

-- | Funcion que dada una proposicion, dice True si es satisfacible,
-- False en otro caso.
esSatisfacible :: Prop -> Bool
esSatisfacible = not . null . modelos

-- | Funcion que dada una proposicion, devuelve su tabla de verdad.
tablaDeVerdad :: Prop -> Tabla
tablaDeVerdad p = [(e, interp p e) | e <- mods]
  where
    mods = estados p

-- | Funcion que devuelve la lista de todos los modelos posibles para
-- una proposición.
modelos :: Prop -> [Estados]
modelos proposicion = filter satisface (evaluaciones (varList proposicion))
    where satisface valor = interp proposicion valor

--------------------------------------------------------------------------------
--------                           AUXILIARES                           --------
--------------------------------------------------------------------------------

-- | Funcion auxiliar de sustituye e interp que devuelve el Name de una Var.
nombre :: Prop -> Name
nombre (Var x) = x

-- | Funcion auxiliar que recibe una variable y regresa sus dos posibles estados.
evaluaciones :: [Name] -> [[Estado]]
evaluaciones []     = [[]]
evaluaciones (x:xs) = map ((x,True):) evalua ++ map ((x,False):) evalua
    where evalua = evaluaciones xs

-- | Funcion auxiliar que calcula el conjunto potencia.
potencia :: Eq a => [a] -> [[a]]
potencia [] = [[]]
potencia (x:xs) = map (x:) pt `union` pt
  where
    pt = potencia xs

-- | Funcion auxiliar que calcula los estados de una proposicion.
estados :: Prop -> [Estados]
estados p = map sort (concatenaElementos (agregaTrue p) (agregaFalse p))

-- | Funcion auxiliar que dada una proposicion da los estados con las variables
-- existentes con valor de True.
agregaTrue :: Prop -> [[Estado]]
agregaTrue p = map (\x -> map (\y -> (y, True)) x) (potencia (varList p))

-- | Funcion auxiliar que dada una proposicion da los estados con las variables
-- restantes con valor de False.
agregaFalse :: Prop -> [[Estado]]
agregaFalse p = map (\x -> map (\y -> (y, False)) x) (reverse (potencia (varList p)))

-- | Funcion auxiliar que dadas dos listas de listas concatena un elemento de
-- la primera con el correspondiente de la segunda.
concatenaElementos :: [[a]] -> [[a]] -> [[a]]
concatenaElementos [] [] = []
concatenaElementos [] (x:xs) = (x:xs)
concatenaElementos (x:xs) [] = (x:xs)
concatenaElementos (x:xs) (y:ys) = [x ++ y] ++ concatenaElementos xs ys

--------------------------------------------------------------------------------
--------                             EJEMPLOS                           --------
--------------------------------------------------------------------------------

imp :: Prop
imp = (Impl (Var "P") (Conj (Var "Q") (Neg (Var "R"))))

syss :: Prop
syss = (Syss (Impl (Var "P") (Var "Q")) (Disy (Neg (Var "P")) (Var "Q")))

varList1 = varList imp
-- Regresa: ["P", "Q", "R"]

varList2 = varList syss
-- Regresa: ["P","Q"]

equivalencia1 = equivalencia imp
-- Regresa: Disy (Neg (Var "P")) (Conj (Var "Q") (Neg (Var "R")))

equivalencia2 = equivalencia (Syss imp imp)
-- Regresa: Conj (Disy (Neg (Disy (Neg (Var "P")) (Conj (Var "Q") (Neg
-- (Var "R"))))) (Disy (Neg (Var "P")) (Conj (Var "Q") (Neg (Var
-- "R"))))) (Disy (Neg (Disy (Neg (Var "P")) (Conj (Var "Q") (Neg (Var
-- "R"))))) (Disy (Neg (Var "P")) (Conj (Var "Q") (Neg (Var "R")))))

negacion1 = negacion imp
-- Regresa: Conj (Var "P") (Disy (Neg (Var "Q")) (Var "R"))

negacion2 = negacion (Neg imp)
-- Regresa: Impl (Var "P") (Conj (Var "Q") (Neg (Var "R")))

sustituye1 = sustituye imp [("P", "A"), ("Q", "B"), ("R", "C")]
-- Regresa: Impl (Var "A") (Conj (Var "B") (Neg (Var "C")))

sustituye2 = sustituye syss [("P", "X"), ("Q", "Y")]
-- Regresa: Syss (Impl (Var "X") (Var "Y")) (Disy (Neg (Var "X")) (Var "Y"))

interp1 = interp imp [("Q",True), ("P",False)]
-- Regresa: True

interp2 = interp imp [("Q",True), ("P",True)]
-- Regresa: error

interp3 = interp imp [("Q",True), ("P",True), ("R",True)]
-- Regresa: False

esTautologia1 = esTautologia syss
-- Regresa: True

esTautologia2 = esTautologia (Conj (Var "P") (Var "Q"))
-- Regresa: False

esContradiccion1 = esContradiccion (Conj (Var "P") (Neg (Var "P")))
-- Regresa: True

esContradiccion2 = esContradiccion imp
-- Regresa: False

esSatisfacible1 = esSatisfacible syss
-- Regresa: True

esSatisfacible2 = esSatisfacible (Conj (Var "P") (Neg (Var "P")))
-- Regresa: False

modelos1 = modelos (Disy (Var "P") (Var "Q"))
-- Regresa: [[("P",True),("Q",True)],[("P",True),("Q",False)],[("P",False),("Q",True)]]

modelos2 = modelos (Conj (Var "P") (Neg (Var "P")))
-- Regresa: []

tablaDeVerdad1 = tablaDeVerdad imp
-- Regresa:
-- [([("P",True),("Q",True),("R",True)],False),
--  ([("P",True),("Q",True),("R",False)],True),
--  ([("P",True),("Q",False),("R",True)],False),
--  ([("P",True),("Q",False),("R",False)],False),
--  ([("P",False),("Q",True),("R",True)],True),
--  ([("P",False),("Q",True),("R",False)],True),
--  ([("P",False),("Q",False),("R",True)],True),
--  ([("P",False),("Q",False),("R",False)],True)]

tablaDeVerdad2 = tablaDeVerdad (Syss (Var "P") (Var "Q"))
-- Regresa:
-- [([("P",True),("Q",True)],True),
--  ([("P",True),("Q",False)],False),
--  ([("P",False),("Q",True)],False),
--  ([("P",False),("Q",False)],True)]
