Lógica computacional
Práctica 03: Formas Normales y Unificación

Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet

Desarrollo de la práctica y dificultades:

Las primeras semanas no nos pudimos poner de acuerdo con respecto a la práctica porque no nos encontrabamos todos los integrantes del equipo y después había que ponerse al corriente con los temas vistos.

Al iniciar la práctica lo que más costo fue entender como se utilizaban las lambdas, en especial en la firma de las funciones, pero una vez asignado el nombre a la lambda dentro de la función fue sencillo usarla, en este caso para cada elemento de la lista recibida.

Para la segunda parte de la práctica era complicado pensar por dónde empezar pero los hints ayudaron bastante e hicieron mucho fácil plantear como organizar las funciones. También en un inicio para cada función auxiliar consideramos todos los casos incuyendo implicaciones y dobles implicaciones pero repasando el archivo de la práctica anterior nos dimos cuenta que había funciones que podíamos usar para simplificarlo, también sirvio demasiado el ejemplo de interiorizaConjuncion vista en labratorio para guiarnos ya que las funciones auxiliares fueron las más que más tardamos en plantear. Y como ya mencionamos las funciones finales de formas normales fueron sencillas usando las funciones auxiliares y algunas de la práctica pasada.

Al final no logramos comunicarnos con un compañero del equipo, pero trabajar en gitlab nos ayudo mucho a sacar de forma limpia y rápido la práctica a pesar de haberla empezado algo tarde. En general no sentimos que se presentaran tantas dificultades como en las prácticas pasadas.
