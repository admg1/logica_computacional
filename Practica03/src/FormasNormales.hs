{-
- Lógica Computacional 2022-2
- Profesor: Francisco Hernández Quiroz
- Ayudante: José Ricardo Desales Santos
- Ayudante: Andrea Regina García Correa
- Laboratorio: Emiliano Galeana Araujo
- Laboratorio: César Eduardo Jardines Mendoza
- Practica 3: Formas Normales y Unificación.
- Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
-}

module FormasNormales where

import LogPro

--------------------------------------------------------------------------------
--------                            FUNCIONES                           --------
--------------------------------------------------------------------------------

-- Función que dada una fórmula f, regresa una fórmula equivalente a f en su Forma Normal Negativa.
formaNormalNegativa :: Prop -> Prop
formaNormalNegativa f = interiorizaNegacion (equivalencia f)

-- Función que dada una fórmula f, regresa una fórmula equivalente a f en su Forma Normal Conjuntiva.
formaNormalConjuntiva :: Prop -> Prop
formaNormalConjuntiva f = interiorizaDisyuncion (formaNormalNegativa f)

-- Función que dada una fórmula f, regresa una fórmula equivalente a f en su Forma Normal Disyuntiva.
formaNormalDisyuntiva :: Prop -> Prop
formaNormalDisyuntiva f = interiorizaConjuncion (formaNormalNegativa f)

--------------------------------------------------------------------------------
--------                           AUXILIARES                           --------
--------------------------------------------------------------------------------

-- Función que regresa una fórmula equivalente donde las negaciones solo se aplican a fórmulas atómicas.
interiorizaNegacion :: Prop -> Prop
interiorizaNegacion (Neg p) = negacion p
interiorizaNegacion (Conj p q) = Conj (interiorizaNegacion p) (interiorizaNegacion q)
interiorizaNegacion (Disy p q) = Disy (interiorizaNegacion p) (interiorizaNegacion q)
interiorizaNegacion p = p

-- Función que regresa una fórmula equivalente donde las disyunciones solo se aplican a disyunciones o literales,
-- suponiendo que la fórmula recibida está en FNN.
interiorizaDisyuncion :: Prop -> Prop
interiorizaDisyuncion (Disy (Conj p q) r) = interiorizaDisyuncion (
                                            Conj
                                            (Disy (interiorizaDisyuncion p) (interiorizaDisyuncion r))
                                            (Disy (interiorizaDisyuncion q) (interiorizaDisyuncion r)))
interiorizaDisyuncion (Disy p (Conj q r)) = interiorizaDisyuncion (
                                            Conj
                                            (Disy (interiorizaDisyuncion p) (interiorizaDisyuncion q))
                                            (Disy (interiorizaDisyuncion p) (interiorizaDisyuncion r)))
interiorizaDisyuncion (Conj p q) = Conj (interiorizaDisyuncion p) (interiorizaDisyuncion q)
interiorizaDisyuncion p = p

-- Función que regresa una fórmula equivalente donde las conjunciones solo se aplican a conjunciones o literales, 
-- suponiendo que la fórmula recibida está en FNN.
interiorizaConjuncion :: Prop -> Prop
interiorizaConjuncion (Conj (Disy p q) r) = interiorizaConjuncion (
                                            Disy
                                            (Conj (interiorizaConjuncion p) (interiorizaConjuncion r))
                                            (Conj (interiorizaConjuncion q) (interiorizaConjuncion r)))
interiorizaConjuncion (Conj p (Disy q r)) = interiorizaConjuncion (
                                            Disy
                                            (Conj (interiorizaConjuncion p) (interiorizaConjuncion q))
                                            (Conj (interiorizaConjuncion p) (interiorizaConjuncion r)))
interiorizaConjuncion (Disy p q) = Disy (interiorizaConjuncion p) (interiorizaConjuncion q)
interiorizaConjuncion p = p

--------------------------------------------------------------------------------
--------                             EJEMPLOS                           --------
--------------------------------------------------------------------------------

f1 = Conj (Impl (Var "P") (Conj (Var "Q") (Neg (Var "R")))) (Var "Q")
f2 = Neg (Syss (Var "P") (Var "Q"))

fnn1 = formaNormalNegativa f1
-- Regresa: ((¬"P" ∨ ("Q" Λ ¬"R")) Λ "Q")

fnn2 = formaNormalNegativa f2
-- Regresa: (("P" Λ ¬"Q") ∨ ("Q" Λ ¬"P"))

fnc1 = formaNormalConjuntiva f1
-- Regresa: (((¬"P" ∨ "Q") Λ (¬"P" ∨ ¬"R")) Λ "Q")

fnc2 = formaNormalConjuntiva f2
-- Regresa: ((("P" ∨ "Q") Λ ("P" ∨ ¬"P")) Λ ((¬"Q" ∨ "Q") Λ (¬"Q" ∨ ¬"P")))

fnd1 = formaNormalDisyuntiva f1
-- Regresa: ((¬"P" Λ "Q") ∨ (("Q" Λ ¬"R") Λ "Q"))

fnd2 = formaNormalDisyuntiva f2
-- Regresa: (("P" Λ ¬"Q") ∨ ("Q" Λ ¬"P"))

