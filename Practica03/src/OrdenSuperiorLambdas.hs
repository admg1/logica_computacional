{-
- Lógica Computacional 2022-2
- Profesor: Francisco Hernández Quiroz
- Ayudante: José Ricardo Desales Santos
- Ayudante: Andrea Regina García Correa
- Laboratorio: Emiliano Galeana Araujo
- Laboratorio: César Eduardo Jardines Mendoza
- Practica 3: Formas Normales y Unificación.
- Integrantes:
- Bernal Núñez Raúl
- Janet Illescas Coria
-}

--------------------------------------------------------------------------------
--------                            FUNCIONES                           --------
--------------------------------------------------------------------------------

-- | Función que dada una función y una lista devuelve otra lista de los resultados de aplicar dicha función a
-- cada elemeto de la lista recibida.
myMap :: (a -> b) -> [a] -> [b]
myMap _ [] = []
myMap f (x:xs) = f x : myMap f xs

-- | Función que dado un predicado y una lista devuelve una lista de los de aquellos elementos que cumplen el predicado.
myFilter :: (a -> Bool) -> [a] -> [a]
myFilter _ [] = []
myFilter f (x:xs)
    | f x = x : myFilter f xs
    | otherwise = myFilter f xs

-- | Función que dada una lista de listas, le sume un elemento (Se asume que siempre es
-- uno) y luego multiplique el resultado por pi.
sumaUnoMultiplicaPi :: Floating a => [[a]] -> [[a]]
sumaUnoMultiplicaPi l = myMap (\x -> myMap (\y -> (y + 1) * pi) x) l

-- | Función que dada una lista de cadenas a cada palabra le agregue el prefijo "pro" y
-- el sufijo "azo".
prefijoSufijo :: [String] -> [String]
prefijoSufijo l = myMap (\x -> "pro" ++ x ++ "azo") l

-- | Función que dada una lista de listas, filtre las que tienen longitud mayor a 4, pero
-- menor a 8.
filtraLongitud :: [[a]] -> [[a]]
filtraLongitud l = myFilter (\x -> length x > 4 && length x < 8) l

-- | Función que dada una cadena (O lista de caracteres), filtre las que son letras, pero
-- que estas no son vocales.
filtraLetrasSinVocales :: String -> String
filtraLetrasSinVocales s = myFilter (\x -> not (esVocal x) && esLetra x) s

--------------------------------------------------------------------------------
--------                           AUXILIARES                           --------
--------------------------------------------------------------------------------

-- | Función que dado un carácter, nos dice si es una vocal (True) o no (False).
esVocal :: Char -> Bool
esVocal c = elem c "aeiouAEIOUáéíóúÁÉÍÓÚ"

-- | Función que dado un carácter, nos dice si es una letra (True) o no (False).
esLetra :: Char -> Bool
esLetra c = elem c "abcdefghiíjklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ"

--------------------------------------------------------------------------------
--------                             EJEMPLOS                           --------
--------------------------------------------------------------------------------

map1 = myMap (*2) [1, 2, 3, 4]
-- Regresa: [2, 4, 6, 8]

map2 = myMap (+5) [3, 6, 4, 5]
-- Regresa: [8, 11, 9, 10]

filter1 = myFilter (>4) [2, 4, 5, 9]
-- Regresa: [5, 9]

filter2 = myFilter (<= 5) [5, 2, 7, 1]
-- Regresa: [5, 2, 1]


lista1 :: [[Float]]
lista1 = [[1,2],[2,6]]

lista2 = [[3.2,4.4,8.3],[3.2,2.6],[6.3,4.2,5.1],[3.2]]

lista3 = ["pulsar", "mover", "venir"]

lista4 = [['s','e','g','u','i','r'], ['b','a','j','o','n']]

lista5 = [[1,3,4,5,3,3], [1,2,5,6], [1,2], [3,7,7,3,4,6], [4,6,4,3,5,34,5,5]]

lista6 = [['a','e'],['j','s','w','o'],['l','a','z','l','q','k']]

lista7 = "¡Hoy no es 10 de Abril!"

lista8 = "El problema no es problema"


sumaUnoMultiplicaPi1 = sumaUnoMultiplicaPi lista1
-- Regresa: [[6.2831855,9.424778],[9.424778,21.99115]]

sumaUnoMultiplicaPi2 = sumaUnoMultiplicaPi lista2
-- Regresa: [[13.194689145077131,16.964600329384883,29.216811678385078],
--          [13.194689145077131,11.309733552923255],
--          [22.933626371205488,16.336281798666924,19.163715186897736],
--          [13.194689145077131]]

prefijoSufijo1 = prefijoSufijo lista3
-- Regresa: ["propulsarazo","promoverazo","provenirazo"]

prefijoSufijo2 = prefijoSufijo lista4
-- Regresa: ["proseguirazo","probajonazo"]

filtraLongitud1 = filtraLongitud lista5
-- Regresa: [[1,3,4,5,3,3],[3,7,7,3,4,6]]

filtraLongitud2 = filtraLongitud lista6
-- Regresa: ["lazlqk"]

filtraLetrasSinVocales1 = filtraLetrasSinVocales lista7
-- Regresa: "Hynsdbrl"

filtraLetrasSinVocales2 = filtraLetrasSinVocales lista8
-- Regresa: "lprblmnsprblm"
