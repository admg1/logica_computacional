Lógica computacional
Práctica 04: Unificación

Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet

Desarrollo de la práctica y dificultades:

Para esta práctica nos pusimos de acuerdo en hacer la mitad de las funciones cada quien.

Para la segunda parte unas funciones requerían usar unas de a primera por lo que se probaron hasta que estas estuvieron, pero no hubo problema en desarrolarlas. En general, hubo funciones que se sintieron más fáciles y rápidas que otras.

En la que más tuvimos problemas para comprenderla y resolverla fue unificaListas, aún no estamos del todo seguros de que este bien pero parece que si. La forma de pensar esa función fue algo complicada y más al inicio el saber que se buscaba hacer exactamente con ella, pero aún después de eso teníamos problemas en los que la función no terminaba, luego salía el resultado al revés y en los casos en que al unificar se regresaba una lista vacía generaba un error que tardamos un rato en resolver.
