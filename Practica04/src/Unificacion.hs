{-
- Lógica Computacional 2022-2
- Profesor: Francisco Hernández Quiroz
- Ayudante: José Ricardo Desales Santos
- Ayudante: Andrea Regina García Correa
- Laboratorio: Emiliano Galeana Araujo
- Laboratorio: César Eduardo Jardines Mendoza
- Practica 4: Unificación.
- Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
-}

module Unificacion where

import Data.List

-- Representa las cadenas.
type Nombre = String

-- Un término es una variable o un símbolo de función seguido de una
-- lista de términos.
data Termino = V Nombre
             | T Nombre [Termino]
             deriving Eq

-- Instancia show para terminos.
instance Show Termino where
  show (V nombre)    = nombre
  show (T nombre []) = nombre
  show (T nombre ts) = nombre ++ concat [show ts]

-- Una variable es un termino.
type Variable = Termino

-- Una sustitución es un par formado por una variable y un término.
type Sustitucion = [(Variable, Termino)]

-- Función que nos dice si un termino es una variable.
esVariable :: Termino -> Bool
esVariable (V x) = True
esVariable _ = False

-- Función que dado un término, regresa su lista de variables.
variables :: Termino -> [Variable]
variables (V x) = [V x]
variables (T x []) = []
variables (T x (y:ys)) = union (variables y) (variablesEnLista ys)

-- Función que regresa la lista de variables dada una lista de términos.
variablesEnLista :: [Termino] -> [Variable]
variablesEnLista = nub . concatMap variables

-- Función que representa a la sustitución identidad.
epsilon :: Sustitucion
epsilon = []

-- Función que dada una sustitución, obtenemos su dominio.
dominio :: Sustitucion -> [Variable]
dominio = map fst

-- Función que dada una sustitución y una variable, regresa la
-- aplicación de la sustitución a la variable.
aplicaVar :: Sustitucion -> Variable -> Termino
aplicaVar [] x = x
aplicaVar (y:ys) x =
  if (fst y == x) then (snd y)
  else aplicaVar ys x

-- Función que dada una sustitución y un término, regresa la
-- aplicación de la sustitución al término.
aplicaT :: Sustitucion -> Termino -> Termino
aplicaT s (V x) = aplicaVar s (V x)
aplicaT s (T f xs) = T f (map (aplicaT s) xs)

-- Función que regresa la sustitución obtenida, eliminando los pares
-- cuyos elementos son iguales.
reduce :: Sustitucion -> Sustitucion
reduce s = filter (\(x,y) -> x /= y) s

-- Función que dadas dos sustituciones, regresa su composición.
composicion :: Sustitucion -> Sustitucion -> Sustitucion
composicion s1 s2 = a ++ b
    where
       a = reduce (map (\(x,y) -> (x, (aplicaT s2 y))) s1)
       b = filter (\(x,y) -> x `notElem` (dominio s1)) s2

-- Función que dados dos términos, regresa la lista formada por el
-- unificador más general de ambos términos. Si no son unificables,
-- regresa la lista vacía.
unifica :: Termino -> Termino -> [Sustitucion]
unifica (V x) (V y) = if (x == y) then [epsilon] else [[(V x, V y)]]
unifica (V x) t = [[(V x, t)] | (V x) `notElem` (variables t)]
unifica t (V y) = [[(V y, t)] | (V y) `notElem` (variables t)]
unifica (T f xs) (T g ys) = if (f == g) then unificaListas xs ys else []

-- Función que regresa la lista formada por el unificador más general
-- de las listas de términos.
unificaListas :: [Termino] -> [Termino] -> [Sustitucion]
unificaListas [] [] = [epsilon]
unificaListas [] _ = []
unificaListas _ [] = []
unificaListas (x:xs) (y:ys) = if (lista1 == [] || lista2 == []) then []
                              else [composicion (head lista2) (head lista1)]
  where
       lista1 = unifica x y
       lista2 = unificaListas lista3 lista4
       lista3 = if (lista1 /= []) then (map (aplicaT (head lista1)) xs) else []
       lista4 = if (lista1 /= []) then( map (aplicaT (head lista1)) ys) else []

---------------------------------------------------------------------------------
--------                             EJEMPLOS                            --------
---------------------------------------------------------------------------------

-- Ejemplos de variables.

x = V "x"
y = V "y"
z = V "z"
u = V "u"
w = V "w"

-- Ejemplos de constantes.

a = T "a" []
b = T "b" []
c = T "c" []

-- Ejemplos de simbolos de función.

f = T "f"
g = T "g"
h = T "h"
p = T "p"

-- Ejemplos de sustituciones
s1 = [(x, a), (z, f [x, y])]
s2 = [(x, z), (y, u)]
s3 = [(z, x), (x, b), (u, c)]
s4 = [(u, f [x]), (y, a)]
s5 = [(x, h [z]), (y, g [b])]

-- Ejemplos de las notas

ejemplo1 = unificaListas [w1] [w2]
w1 = f [w, f [x, h [z]]]
w2 = f [g [x], f [x, y]]

ejemplo2 = unificaListas [y1] [y2]
y1 = p [x, f [y]]
y2 = p [g [y, a], f [b]]

-- ejemplos funciones

esVariable1 = esVariable x
-- Regresa: True

esVariable2 = esVariable a
-- Regresa: False

variables1 = variables (g [f [x,y], z])
-- Regresa: [x,y,z]

variables2 = variables (f [w, f [x, h [z]]])
-- Regresa: [w,x,z]

variablesEnLista1 = variablesEnLista [f [x,y], g [f [x, y], z]]
-- Regresa: [x,y,z]

variablesEnLista2 = variablesEnLista [f [x], g [y, a], w, z]
-- Regresa: [x,y,w,z]

dominio1 = dominio s1
-- Regresa: [x,z]

dominio2 = dominio s2
-- Regresa: [x,y]

aplicaVar1 = aplicaVar s1 x
-- Regresa: a

aplicaVar2 = aplicaVar s1 y
-- Regresa: y

aplicaVar3 = aplicaVar s1 z
-- Regresa: f [x, y]

aplicaT1 = aplicaT s1 (g [f [x, y], z])
-- Regresa: g [f [a, y], f [x, y]]

aplicaT2 = aplicaT s2 (f [w, f [x, h [z]]])
-- Regresa: f [w, f [z, h [z]]]

reduce1 = reduce [(x,a), (y,y), (z, f [x,y])]
-- Regresa: [(x,a), (z, f [x,y])]

reduce2 = reduce [(x, f[h [z]]), (w, w)]
-- Regresa: [(x, f[ h[z]])]

composicion1 = composicion s2 s3
-- Retresa: [(y, c), (z, x), (u, c)]

composicion2 = composicion s4 s5
-- Retresa: [(u, f [h [z]]), (y, a), (x, h [z])]

unifica1 = unifica a a
-- Regresa: [[]]

unifica2 = unifica x a
-- Regresa: [[(x, a)]]

unifica3 = unifica x (f[y])
-- Regresa: [[(x, f[y])]]

unifica4 = unifica x (f[x])
-- Regresa: []

unifica5 = unifica (f[y]) x
-- Regresa: [[(x, f[y])]]

unifica6 = unifica (f[x]) x
-- Regresa: []

unificaListas1 = unificaListas [x, f[x], y] [a, y, z]
-- Regresa: [[(z, f[a]), (y, f[a]), (x, a)]]

unificaListas2 = unificaListas [x, f[x]] [y, y]
-- Regresa: []
