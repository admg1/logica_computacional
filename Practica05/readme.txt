Lógica computacional
Práctica 05: Introducción a Prolog.

Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet

Desarrollo de la práctica y dificultades:

Para esta práctica decidimos repartirnos los ejercicios, uno haría la mitad de la parte Izi, y los de Listas, y otro haría la otra mitad de Izi y los de Árboles. Asimismo, en caso de que tuvieramos algún problema con los ejercicios pues nos ayudaríamos.

Por otra parte, una de las dificultades fue entender la sintaxis de Prolog, sin embargo, nos terminamos acostumbrando. En cuestión de los ejercicios, tuvimos problemas sobretodo con el ejercicio de los circuitos lógicos, pues la manera de implementar and y not estaban provocando un error, pero al final lo resolvimos de otra manera y funcionó de manera correcta. 

Igualmente, tuvimos problema con la parte de eliminar un elemento de una lista, pues leyendo el ejercicio no sabíamos si era eliminar su primera aparición, o todas las apariciones, por lo que recurrimos a Emiliano, y nos dijo que lo hicieramos de la manera que nos pareciera más interesante o más fácil. Asimismo, tuvimos problemas para entender que pedían algunos ejercicios pero Emiliano nos lo aclaró.
