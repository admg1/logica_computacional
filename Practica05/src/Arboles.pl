/*
- Lógica Computacional 2022-2
- Profesor: Francisco Hernández Quiroz
- Ayudante: José Ricardo Desales Santos
- Ayudante: Andrea Regina García Correa
- Laboratorio: Emiliano Galeana Araujo
- Laboratorio: César Eduardo Jardines Mendoza
- Practica 5: Introducción a Prolog.
- Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
*/


/*
 * Predicado de ser árbol binario.
 */
bt(void).
bt(node(A, T1, T2)) :- integer(A), bt(T1), bt(T2).

/*
 * Predicado que verifica si dado un elemento y un árbol,
 * este se encuentra en el árbol.
 */
elem(A, bt(node(X, I, D))) :- A = X ; elem(A, I) ; elem(A, D).

/*
 * Predicado que verifica si dado un elemento y un árbol,
 * este es menor que todos los elementos del árbol.
 */
minelem(_, bt(void)).
minelem(A, bt(node(X, I, D))) :- integer(A), A < X, minelem(A, I), minelem(A, D).


/* Ejemplos */

elem1 :- elem(5, bt(node(1, bt(node(3, bt(node(5, bt(void), bt(void))), bt(void))), bt(void)))).
% Regresa: true

elem2 :- elem(2, bt(node(3, bt(node(4, bt(void), bt(void))), bt(void)))).
% Regresa: false


minelem1 :- minelem(5, bt(node(1, bt(node(3, bt(node(5, bt(void), bt(void))), bt(void))), bt(void)))).
% Regresa: false

minelem2 :- minelem(1, bt(node(6, bt(node(3, bt(node(2, bt(void), bt(void))), bt(void))), bt(void)))).
% Regresa: true
