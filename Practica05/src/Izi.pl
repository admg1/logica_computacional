/*
- Lógica Computacional 2022-2
- Profesor: Francisco Hernández Quiroz
- Ayudante: José Ricardo Desales Santos
- Ayudante: Andrea Regina García Correa
- Laboratorio: Emiliano Galeana Araujo
- Laboratorio: César Eduardo Jardines Mendoza
- Practica 5: Introducción a Prolog.
- Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
*/


/*
 * Predicado que representa la sucesión de tribonacci.
 */
trib(0, 0).
trib(1, 0).
trib(2, 1).
trib(X, Y) :-
    X > 2,
    X1 is X - 1,
    X2 is X - 2,
    X3 is X - 3,
    trib(X1, Y1),
    trib(X2, Y2),
    trib(X3, Y3),
    Y is Y1 + Y2 + Y3.

/*
 * Predicado que dado un número, regresa su factorial.
 */
fact(0, 1).
fact(1, 1).
fact(X, Y) :-
    X > 1,
    X1 is X - 1,
    fact(X1, Y1),
    Y is X * Y1.

/*
 * Predicado que dado un número, calcula la suma de todos los
 * naturales hasta dicho número.
 */
cuenta(0, 0).
cuenta(1, 1).
cuenta(X, Y) :-
    X > 1,
    X1 is X - 1,
    cuenta(X1, Y1),
    Y is X + Y1.

/*
 * Predicado que dada una lista regresa la suma de todos los
 * elementos de la lista.
 */
suma([], 0).
suma([P|R], Sum) :- integer(P), suma(R, S), Sum is P + S.

/*
 * Predicado que regresa true si las entradas son true y false
 * si alguna de ellas no lo es.
 */
and(X, Y) :- X, Y.

/*
 * Predicado que regresa true si la entrada es false y false en
 * caso contrario.
 */
not(X) :- \+ X.

/*
 * Relación entre entrada y salida del primer circuito.
 */
circuito1(X, Y) :- and(not(and(not(X), Y)), Y).

/*
 * Relación entre entrada y salida del segundo circuito.
 */
circuito2(X, Y, Z) :- and(not(and(not(X), X)), and(Y, Z)).


/* Ejemplos */

trib1(Y) :- trib(6, Y).
% Regresa: Y = 7

trib2(Y) :- trib(13, Y).
% Regresa: Y = 504


fact1(Y) :- fact(8, Y).
% Regresa: Y = 40320

fact2(Y) :- fact(11, Y).
% Regresa: Y = 39916800


cuenta1(Y) :- cuenta(12, Y).
% Regresa: Y = 78

cuenta2(Y) :- cuenta(1000, Y).
% Regresa: Y = 500500


suma1(Y) :- suma([33,214,34,45,645], Y).
% Regresa: Y = 971

suma2(Y) :- suma([24,35,23,30], Y).
% Regresa: Y = 112


c1r1 :- circuito1(true, true).
% Regresa: true

c1r2 :- circuito1(true, false).
% Regresa: false

c1r3 :- circuito1(false, true).
% Regresa: false

c1r4 :- circuito1(false, false).
% Regresa: false


c2r1 :- circuito2(true,true,true).
% Regresa: true

c2r2 :- circuito2(true,true,false).
% Regresa: false

c2r3 :- circuito2(true,false,true).
% Regresa: false

c2r4 :- circuito2(true,false,false).
% Regresa: false

c2r5 :- circuito2(false,true,true).
% Regresa: true

c2r6 :- circuito2(false,true,false).
% Regresa: false

c2r7 :- circuito2(false,false,true).
% Regresa: false

c2r8 :- circuito2(false,false,false).
% Regresa: false.

