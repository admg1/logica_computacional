/*
- Lógica Computacional 2022-2
- Profesor: Francisco Hernández Quiroz
- Ayudante: José Ricardo Desales Santos
- Ayudante: Andrea Regina García Correa
- Laboratorio: Emiliano Galeana Araujo
- Laboratorio: César Eduardo Jardines Mendoza
- Practica 5: Introducción a Prolog.
- Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
*/


/*
 * Predicado que verifica si dada una lista y un objeto, este
 * se encuentra en la lista.
 */
pertenece([H|T], X) :- H = X ; pertenece(T, X).

/*
 * Predicado que dado un objeto y una lista, este borra la primera
 * aparición del elemento de la lista.
 */
elimina(_, [], []).
elimina(X, [X|T], T).
elimina(X, [H|T], [H|T2]) :- H \= X, elimina(X, T, T2).


/* Ejemplos */

pertenece1 :- pertenece([w,s,a,z,s,d], c).
% Regresa: false

pertenece2 :- pertenece([12,34,12,3,43,2,4,6], 4).
% Regresa: true


elimina1(L) :- elimina(a, [z,d,s,f,a], L).
% Regresa: L = [z, d, s, f]

elimina2(L) :- elimina(3, [4,34,2,3,5,6,3], L).
% Regresa: L = [4, 34, 2, 5, 6, 3]
