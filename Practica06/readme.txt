Lógica computacional
Práctica 06: Problemas con Prolog.

Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet

Desarrollo de la práctica y dificultades:

Para esta práctica lo que hicimos fue repartirnos un problema cada integrante. Asimismo, en cuanto a las dificultades, en el problema 2, teníamos dudas de lo que pedía el ejercicio, y cuáles eran las relaciones que estaban restringidas pues simplemente se podría poner abuelo(prota, prota). con lo que se podría contestar abuelo(X, X). pero Emiliano dijo que de preferencia esa relación estuviera restringida. 

Por otra parte, uno de los mayores problemas fue entrar en ciclos infinitos, sin embargo, se resolvió pensar el problema desde la relación abuelo(X, Y) y partir desde lo que esta necesita para hacer las demás y no al revés. Además de tener el árbol genealógico dibujado para tener una idea más clara de lo que se necesita.

Para el problema 1 se nos dificulto pensar en cómo plantearlo para empezar. Nos guiamos de los ejemplos de juegos donde fue algo sencillo, raro y curioso pensar, más que nada en cuáles iban a ser nuestros estados.
