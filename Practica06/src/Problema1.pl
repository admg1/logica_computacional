/*
- Lógica Computacional 2022-2
- Profesor: Francisco Hernández Quiroz
- Ayudante: José Ricardo Desales Santos
- Ayudante: Andrea Regina García Correa
- Laboratorio: Emiliano Galeana Araujo
- Laboratorio: César Eduardo Jardines Mendoza
- Practica 6: Problemas en Prolog..
- Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
*/

/* Cuartos entre los que nos podemos mover, del primer cuarto es posible pasar al segundo cuarto. */

pasar(a, b).
pasar(b, c).
pasar(b, e).
pasar(b, a).
pasar(c, d).
pasar(c, b).
pasar(d, e).
pasar(d, c).
pasar(e, f).
pasar(e, g).
pasar(e, b).
pasar(e, d).
pasar(f, e).
pasar(g, e).

/* Cuartos por los que no debemos pasar para llegar a g. */

invalid(state(d)).
invalid(state(f)).

/* Pasamos de un cuarto C1 a C2, estabamos en C1 y al movernos cambiamos el estado a C2.
   Esto siempre y cuando podamos pasar de C1 a C2. */

mov(pasar(C1, C2), state(C1), state(C2)) :- pasar(C1, C2).

/* Busca el camino para llegar del estado Init a End,
   verificando que no pase por un estado inválido o alguno antes probado. */

path(Init, Init, _, []).
path(Init, End, Visited, [pasar(C1, C2)| Path]) :-
    mov(pasar(C1, C2), Init, Qn),
    \+ invalid(Qn),
    \+ member(Qn, Visited),
    path(Qn, End, [Qn | Visited], Path).

/* Busca el camino de a hacia el cuarto donde está el telefono (g). */

caminoTel(Path) :- path(state(a), state(g), [], Path).

% Regresa: Path = [pasar(a, b), pasar(b, e), pasar(e, g)]
