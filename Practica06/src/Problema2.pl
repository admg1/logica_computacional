/*
- Lógica Computacional 2022-2
- Profesor: Francisco Hernández Quiroz
- Ayudante: José Ricardo Desales Santos
- Ayudante: Andrea Regina García Correa
- Laboratorio: Emiliano Galeana Araujo
- Laboratorio: César Eduardo Jardines Mendoza
- Practica 6: Problemas en Prolog.
- Integrantes:
- Bernal Núñez Raúl
- Illescas Coria Janet
*/


/*
 * Visualización del árbol:
 *
 *    Alberto
 *        \
 *      (Prota)
 *        Juan <3 Liliana
 *          \      /    \
 *          Antonio     Sofía <3 Alberto
 *                         \     /    \
 *                           Ana      Juan <3 Liliana
 *                                      \      /   \
 *                                      Antonio     Sofía <3 Alberto
 *                                                     \     /    \
 *                                                       Ana      ...
 */



/*
 * Definimos los siguientes hechos
 */


/* Juan (protagonista) es pareja de Liliana.
   Alberto es pareja de Sofía. */
pareja(juan, liliana).
pareja(alberto, sofia).

/* Alberto es padre de Juan.
   Alberto es padre de Ana.
   Juan es padre de Antonio. */
padre(alberto, juan).
padre(alberto, ana).
padre(juan, antonio).

/* Liliana es madre de Sofía.
   Liliana es madre de Antonio.
   Sofía es madre de Ana. */
madre(liliana, sofia).
madre(liliana, antonio).
madre(sofia, ana).

/* Juan es hombre.
   Alberto es hombre.
   Antonio es hombre. */
hombre(juan).
hombre(alberto).
hombre(antonio).

/* Liliana es mujer.
   Sofía es mujer.
   Ana es mujer. */
mujer(liliana).
mujer(sofia).
mujer(ana).



/*
 *  Definimos las reglas de las relaciones familiares
 */


/* Predicado para la relación de abuelo. */
abuelo(X, Y) :- hombre(X), padreAux(X, Z), padreAux(Z, Y).

/* Predicado para la relación de abuela. */
abuela(X, Y) :- mujer(X), padreAux(X, Z), padreAux(Z, Y).

/* Predicado auxiliar de que incluye padre, madre, padrastro y madrastra,
   para que funcione el acertijo. */
padreAux(X, Y) :- padre(X, Y); madre(X, Y); padrastro(X, Y); madrastra(X, Y).

/* Predicado para la relación de padrastro. */
padrastro(X, Y) :- hombre(X), pareja(X, Z), madre(Z, Y).

/* Predicado para la relación de madrastra. */
madrastra(X, Y) :- mujer(X), pareja(Z, X), padre(Z, Y).



/*
 * Ejemplos
 */

abuelo1(X) :- abuelo(X, X).
% Regresa: X = juan
